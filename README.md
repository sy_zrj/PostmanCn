# PostmanCn
最新中文版本：8.0.9  
更新时间：2021-03-27  
[下载Postman](#下载Postman)  
[点击下载中文版](https://gitee.com/hlmd/PostmanCn/releases)


## 帮助
QQ群：[494969115](https://jq.qq.com/?_wv=1027&k=WAheqTCx)  
不使用联网工作区，可以点开右上角设置图标中的便笺(Scratch Pad)模式


## 安装中文版
### Windows
**app.rar|zip**  
解压&&复制app文件夹到resources目录下即可(PS:~为应用程序安装目录，默认在C:/Users/用户名/AppData/Local/Postman)  
~/Postman/app-\*.\*.\*/resources/

**app-\*.\*.\*.rar|zip**  
解压直接使用

### Mac
解压&&替换app目录  
目录位置 ~/Postman.app/Contents/Resources/app

### Linux
解压&&替换app目录




## 下载Postman

### 最新版本下载
https://www.postman.com/downloads/


## 历史版本下载

### Windows
**Win64** https://dl.pstmn.io/download/version/版本号/win64  
**Win32** https://dl.pstmn.io/download/version/版本号/win32

### Mac
https://dl.pstmn.io/download/version/版本号/osx

### Linux
https://dl.pstmn.io/download/version/版本号/linux
